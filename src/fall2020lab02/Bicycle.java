//Jamin Huang 1938040
package fall2020lab02;

public class Bicycle {
	private String manufacturer;
	private int numOfGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer,int numOfGears,double maxSpeed) {
		this.manufacturer=manufacturer;
		this.numOfGears=numOfGears;
		this.maxSpeed=maxSpeed;
	}
	public String toString() {
		return "Manufacturer: "+manufacturer+", Number of Gears: "+numOfGears+", Max Speed: "+maxSpeed;
	}
}