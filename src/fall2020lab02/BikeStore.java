//Jamin Huang 1938040
package fall2020lab02;

public class BikeStore {

	public static void main(String[] args) {
		
		Bicycle[] bikearr=new Bicycle[4];
		
		bikearr[0]=new Bicycle("Honda",4,35.8);
		bikearr[1]=new Bicycle("Toyota",6,50.5);
		bikearr[2]=new Bicycle("Mazda",3,25.5);
		bikearr[3]=new Bicycle("Chevrolet",4,36.2);
		
		
		for(int i=0;i<bikearr.length;i++) {
			System.out.println(bikearr[i].toString());
		}
	}

}
